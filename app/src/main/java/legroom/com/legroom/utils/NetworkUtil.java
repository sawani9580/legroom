package legroom.com.legroom.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.view.Gravity;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Sourabh on 4/7/17.
 */

public class NetworkUtil {

    public static final String DOMAIN = "https://app.legrooms.com";

    public static final String LOGIN = "/api/authenticate";
    public static final String MEETING = "/api/listing/Meeting/Bangalore";


    public static final String EXCEPTION = "EXCEPTION";
    public static final String NO_INTERNET = "NO_INTERNET";
    public static final int RESPONSE_412 = 412;
    public static final int RESPONSE_500 = 500;


    public static class NetworkConnection extends AsyncTask<Void, Void, String> {

        private Context context;
        private String uri;
        private JSONObject params;
        private ResponseCallBack responseCallBack;
        private int responseCode;
        private int connectionId;
        private String header;

        public interface ResponseCallBack {
            void responseCallBack(String response, int responseCode, int connectionId);
        }

        public NetworkConnection(Context context, String uri, JSONObject params, int connectionId, String header) {
            this.context = context;
            this.uri = uri;
            this.params = params;
            responseCallBack = (ResponseCallBack) context;
            this.connectionId = connectionId;
            this.header = header;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            if (!isNetworkAvailable(context)) {
                return NO_INTERNET;
            }
            HttpURLConnection con;
            String responseTxt;
            try {
                URL x = new URL(DOMAIN + uri);
                //Log.e("Hitting URL****", DOMAIN + uri);
                con = (HttpURLConnection) x.openConnection();
                if (header != null) {
                    con.addRequestProperty("authorization", "bearer " + header);
                    con.addRequestProperty("content-type", "application/json");
                }
                con.setReadTimeout(10000);
                con.setConnectTimeout(10000);
                con.setRequestMethod("POST");
                con.setDoInput(true);
                con.setDoOutput(true);
                if (this.params != null) {
                    OutputStream os = con.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8"));
                    writer.write(this.params.toString());
                    writer.flush();
                    writer.close();
                }
                con.connect();
                responseCode = con.getResponseCode();
                //Log.e("response code****", "" + responseCode);
                InputStream inputstream;
                if (responseCode == 200) {
                    inputstream = con.getInputStream();
                } else {
                    inputstream = con.getErrorStream();
                }
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputstream));
                String line;
                StringBuilder builder = new StringBuilder();
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
                responseTxt = builder.toString();
                inputstream.close();
                con.disconnect();
            } catch (Exception e) {
                e.printStackTrace();
                responseTxt = EXCEPTION;
            }
            return responseTxt;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            responseCallBack.responseCallBack(s, responseCode, connectionId);
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        try {
            if (context != null) {
                ConnectivityManager connectivityManager = (ConnectivityManager) context
                        .getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                return activeNetworkInfo != null && activeNetworkInfo.isConnected();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void displayMessageToast(String message, Context context) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

}
