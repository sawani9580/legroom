package legroom.com.legroom.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Sourabh on 4/7/17.
 */


public class PrefUtils {

    public static final String LOGIN_DETAILS = "login_details";
    public static final String TOKEN = "token";
    public static final String USERNAME = "mobile";
    public static final String PASSWORD = "password";


    public static boolean insertUpdatePrefString(Context context, String prefName, String key, String value) {
        if(context != null) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(key, value);
            editor.apply();
            return true;
        }
        return false;
    }

    public static String fetchPrefString(Context context, String prefName, String key) {
        if(context != null) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
            return sharedPreferences.getString(key, null);
        }
        return null;
    }

}
