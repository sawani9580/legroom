package legroom.com.legroom;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import org.json.JSONObject;

import legroom.com.legroom.utils.NetworkUtil;
import legroom.com.legroom.utils.PrefUtils;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        NetworkUtil.NetworkConnection.ResponseCallBack {

    private ImageView image;
    private EditText userName;
    private EditText pass;
    private Button login;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        userName = (EditText) findViewById(R.id.userName);
        pass = (EditText) findViewById(R.id.pass);
        login = (Button) findViewById(R.id.submit);
        login.setOnClickListener(this);
        image = (ImageView) findViewById(R.id.arrow);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }

    @Override
    public void onClick(View view) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("email", userName.getText().toString());
            jsonObject.put("password", pass.getText().toString());
            Log.e("payload-->>", jsonObject.toString());
            login.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            new NetworkUtil.NetworkConnection(this, NetworkUtil.LOGIN, jsonObject, 0, "069b10d4-1fa1-dd4c-15b8-736da4cbc78a").execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void responseCallBack(String response, int responseCode, int connectionId) {
        Log.e("response-->>", response);
        login.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        try {
            if (response.equals(NetworkUtil.EXCEPTION)) {
                NetworkUtil.displayMessageToast("Something went wrong", this);
                return;
            } else if (response.equals(NetworkUtil.NO_INTERNET)) {
                //progressDialog.show();
                //ConnectionUtil.displayMessage("No Internet", this);
                return;
            } else if (responseCode == NetworkUtil.RESPONSE_412 || responseCode == NetworkUtil.RESPONSE_500) {
                JSONObject jsonObject = new JSONObject(response);
                NetworkUtil.displayMessageToast(jsonObject.getString("message"), this);
                return;
            } else {
                if (connectionId == 0) {
                    JSONObject json = new JSONObject(response);
                    if (json.has("token")) {
                        PrefUtils.insertUpdatePrefString(this,
                                PrefUtils.LOGIN_DETAILS, PrefUtils.TOKEN, json.getString("token"));
                        PrefUtils.insertUpdatePrefString(this,
                                PrefUtils.LOGIN_DETAILS, PrefUtils.USERNAME, userName.getText().toString());
                        PrefUtils.insertUpdatePrefString(this,
                                PrefUtils.LOGIN_DETAILS, PrefUtils.PASSWORD, pass.getText().toString());

                        Intent intent = new Intent(this, MapLayout.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
